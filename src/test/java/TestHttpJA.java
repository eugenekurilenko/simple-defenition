import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Keys;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class TestHttpJA {

    String to = "[stroke-dasharray='5 5']";

    @Test
    void runSimpleDef() {

        //Открыть тестовый контур

        open("https://cloud-studio-pilot-stage.platformeco.lmru.tech/");
        $$(".form__title").last().shouldHave(text("Sign in"));
        $("#ldap_login").setValue("60101637");
        $("#ldap_password").setValue("Shadowfiend24");
        $$(".form__button").last().click();
        $("[alt='Platformeco Logo']").shouldBe(visible);

        //Создание новой сессии

        $(".button.is-medium.is-primary.is-rounded").click();
        $("[placeholder='Session Title']").setValue("Test Session");
        $("[placeholder='Git repository']").setValue("https://gitlab.lmru.adeo.com/api-manager-channels/golden-channel/golden-channel.git");
        $("[placeholder='Git ref']").sendKeys(Keys.CONTROL + "a");
        $("[placeholder='Git ref']").sendKeys(Keys.BACK_SPACE);
        $("[placeholder='Git ref']").setValue("ethalon");
        $("[placeholder='Username']").sendKeys(Keys.CONTROL + "a");
        $("[placeholder='Username']").sendKeys(Keys.BACK_SPACE);
        $("[placeholder='Username']").setValue("60101637");
        $("[placeholder='Email']").sendKeys(Keys.CONTROL + "a");
        $("[placeholder='Email']").sendKeys(Keys.BACK_SPACE);
        $("[placeholder='Email']").setValue("evgeniy.kurilenko@leroymerlin.ru");
        $("[placeholder='Token']").setValue("cs8Pq5nUfynStnz29Krj");
        $$(".button.is-primary").last().click();
        $(".icon.has-text-success").should(appear, Duration.ofSeconds(150));


        $(".mdi.mdi-play").click();
        $("[data-testid=projectVersion]").shouldHave(text("0.0.1"));

        //Создание дефиниции

        $("[data-testid=button]").click();
        $("[data-testid=createOptionsList-0]").click();
        $(".mb-m3.title__3ByWh").shouldHave(exactText("Definition"));
        $("[data-testid=creationForm__logicalUnit-name]").setValue("Test-Definition");
        $(".button__1pGBj.access__3Tdza.M__zdUR7").click();
        $("[placeholder=Search]").setValue("Test-Definition");
        $(".Table_row__329lz").click();

        //Настройки

        $$(".input__2C-cG").last().setValue("/testpath");
        $$(".content__3YlxX").first().click();
        $("[title=Yes]").click();
        $$(".input__2C-cG").last().shouldHave(value("/testpath"));

        //Документация

        $("[data-testid=documentation]").click();
        $$(".content__3YlxX").get(4).click();
        $(".title__3ByWh").shouldHave(exactText("Edit"));
        $$(".content__3YlxX").get(5).click();
        $("[name=name]").sendKeys(Keys.CONTROL + "a");
        $("[name=name]").sendKeys(Keys.BACK_SPACE);
        $("[name=name]").setValue("test-param");
        $$(".S__3lfUR.secondary__2yOuh.mr-s3").last().click();
        $$(".input__2ycD5").get(9).sendKeys(Keys.CONTROL + "a");
        $$(".input__2ycD5").get(9).sendKeys(Keys.BACK_SPACE);
        $$(".input__2ycD5").get(9).setValue("500");
        $$(".input__2ycD5").get(10).setValue("NOT OK");
        $("[title=Save]").click();
        $("[title=Yes]").click();
        $$(".content__3YlxX").get(4).click();
        $(".parameter__name").shouldHave(exactText("test-param"));
        $$(".response-col_status").get(1).shouldHave(exactText("200"));
        $$(".response-col_status").last().shouldHave(exactText("500"));
        $$(".markdown").first().shouldHave(exactText("OK"));
        $$(".markdown").last().shouldHave(exactText("NOT OK"));

        //Flow
    }


         @Test
         void dragNdropSwitcher() {
            SelenideElement element = $("[data-testid='switch']");
            actions()
                    .moveToElement(element)
                    .clickAndHold()
                    .pause(2)
                    .perform();
            $("[data-testid='switch']").dragAndDropTo(to);
    }


        //Разлогин

 //       $("[title=Exit]").click();
 //       $("[alt='Platformeco Logo']").shouldBe(visible);
 //       $(".mdi.mdi-delete").click();
 //       $(".subtitle.is-5.has-text-grey.has-text-centered").shouldHave(exactText("No sessions yet"));

  //      sleep(20000);
    }

